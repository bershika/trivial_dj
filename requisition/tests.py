from django.test import TestCase
from django.utils import timezone
from django.core.validators import RegexValidator, ValidationError

from .models import Requisition

class RequisitionMethodTests(TestCase):

    def test_min_requisition_create_by_managers(self):
        """
        Manager will happily save requisitions with minimum data
        """
        Requisition.objects.create(create_date=timezone.now())

        self.assertEqual(Requisition.objects.count(), 1)

    def test_min_requisition_create_by_save(self):
        """
        Obj ``save`` happily saves with no data
        """
        new_req = Requisition(create_date=timezone.now())

        new_req.save()

        self.assertEqual(Requisition.objects.count(), 1)

    def test_non_alphanum_patient_name(self):
        """
        Cannot save Requisition with invalid patient names???
        """
        new_req = Requisition(patient_name='&6', create_date=timezone.now())
        new_req.save()

        self.assertEqual(Requisition.objects.count(), 1)

    def test_full_clean_on_patient_name(self):
        """
        Have to call full_clean to find validation errors
        """
        new_req = Requisition(patient_name='&6', create_date=timezone.now())

        with self.assertRaises(ValidationError):
            new_req.full_clean()
