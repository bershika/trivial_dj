from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms import ModelForm
from  django.core.validators import RegexValidator, ValidationError

class Requisition(models.Model):
	patient_name = models.CharField(max_length=6, validators=[
			RegexValidator(
				regex='^[a-zA-Z0-9]*$',
				message='Name must be Alphanumeric',
				code='invalid_patient_name'
			),
		]
	)
	patient_age = models.IntegerField(default=0)
	create_date = models.DateTimeField('date created', null = False)

class RequisitionForm(ModelForm):
	class Meta:
		model = Requisition
		readonly_fields=('id',)
		fields = ['patient_name', 'patient_age']
		labels = {
			'patient_name': _('Name'),
		}
		help_texts = {
			'patient_name': _('Some useful help text.'),
		}
		error_messages = {
			'patient_name': {
				'max_length': _("Name is too long."),
			},
		}

