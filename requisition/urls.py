from django.conf.urls import url, include

from . import views

urlpatterns = [
	# ex: /requisition/
    url(r'^$', views.index, name='index'),
    # ex: /requisition/5/
    url(r'^(?P<requisition_id>[0-9]+)/$', views.detail, name='detail'),
    # ex: /requisition/5/results/
    url(r'^(?P<requisition_id>[0-9]+)/results/$', views.results, name='results'),
    # ex: /requisition/submit/
    url(r'^submit/$', views.submit, name='submit'),

    url(r'^login/$', views.auth, name='login'),
    url(r'^authenticate/$', views.do_auth, name='authenticate'),
]