from django.shortcuts import get_object_or_404, render, redirect
from django.template import loader
from django.http import HttpResponse
from django.utils import timezone
from django.core.urlresolvers  import reverse
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib import messages
from requests_oauthlib import OAuth2Session

from .models import Requisition, RequisitionForm
from .backends import AuthenticationBackend

@login_required
def index(request):
	latest_requisition_list = Requisition.objects.order_by('-create_date')[:100]
	template = loader.get_template('requisition/index.html')
	context = {
		'latest_requisition_list': latest_requisition_list,
	}
	return HttpResponse(template.render(context, request))

def detail(request, requisition_id):
	requisition = get_object_or_404(Requisition, pk=requisition_id)
	return render(request, 'requisition/detail.html', {'requisition': requisition})

def results(request, requisition_id):
	requisition = get_object_or_404(Requisition, pk=requisition_id)
	messages.add_message(request, messages.SUCCESS, '"Thank you for your submission. Reviewing has started."')

	return render(request, 'requisition/detail.html', {'requisition': requisition})

def submit(request):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = RequisitionForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			req = form.save(commit=False)
			req.create_date = timezone.now()
			req.save()
			# redirect to a new URL:
			return redirect(results, requisition_id=req.id)
		else: 
			return render(request, 'requisition/submit.html', {'form': form})

	# if a GET (or any other method) we'll create a blank form
	else:
		name = 'New Requisition'
		form = RequisitionForm()

		return render(request, 'requisition/submit.html', {'form': form, 'name': name})


def auth(request):
	client_id = settings.OAUTH_CLIENT_ID
	authorization_base_url = settings.OAUTH_AUTH_URL

	github = OAuth2Session(client_id=client_id)
	authorization_url, request.session['oauth_state'] = github.authorization_url(authorization_base_url)
	return render(request, 'requisition/login.html', {'authorization_url': authorization_url})

def do_auth(request):
	client_id = settings.OAUTH_CLIENT_ID
	client_secret = settings.OAUTH_AUTH_SECRET
	token_url = settings.OAUTH_TOKEN_URL
	state = request.session.get('oauth_state')
	token = request.session.get('oauth_token')
	authorization_response = request.build_absolute_uri()
	try:
		if token:
			github = OAuth2Session(client_id=client_id, state=state, token=token)
		else:
			github = OAuth2Session(client_id=client_id, state=state)
			token = github.fetch_token(token_url, client_secret=client_secret, authorization_response=authorization_response)
			request.session['oauth_token'] = token
		if token:
			r = github.get(settings.OAUTH_LOGIN_RESOURCE)
			username = r.json().get('login')
			if not username:
				raise Exception('Could not verify your credentials')
			user = authenticate(username=username) or request.user
			if not user or user.is_anonymous:
				user = User(username=username)
				user.set_unusable_password()
				user.save()
			login(request, user, settings.USER_ATHENTICATION_BACKEND)

	except Exception as e:
		authorization_base_url = settings.OAUTH_AUTH_URL
		authorization_url, request.session['oauth_state'] = github.authorization_url(authorization_base_url)
		messages.add_message(request, messages.ERROR,
			"Something went wrong. Please try again: <a href='%s'>Login with you GitHub account</a><br/>'%s'" % (authorization_url, e),
			extra_tags='danger safe')
	
	return render(request, 'requisition/authenticate.html')
