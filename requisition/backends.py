from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

class AuthenticationBackend(ModelBackend):

    def authenticate(self, **credentials):
        return self._authenticate_by_username(**credentials)

    def _authenticate_by_username(self, **credentials):
        username = credentials.get('username')

        if username is None:
            return None
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    # def _authenticate_by_email(self, **credentials):
    #     # Even though allauth will pass along `email`, other apps may
    #     # not respect this setting. For example, when using
    #     # django-tastypie basic authentication, the login is always
    #     # passed as `username`.  So let's place nice with other apps
    #     # and use username as fallback
    #     email = credentials.get('email', credentials.get('username'))
    #     if email:
    #         for user in filter_users_by_email(email):
    #             if user.check_password(credentials["password"]):
    #                 return user
    #     return None