README.txt

python -V
3.5.2

#Clone Repo
git clone https://bershika@bitbucket.org/bershika/trivial_dj.git

#Global pkg
sudo apt-get install nginx
sudo apt-get install python-pip
sudo apt-get install python-virtualenv

#Local env
cd trivial_dj
virtualenv --python=`which python3` .
source bin/activate
pip install gunicorn 
pip install -r requirements.txt 

#Start app
python manage.py runserver
export OAUTHLIB_INSECURE_TRANSPORT=1 #To get OauthLib work without https 

#Test
python manage.py test requisition

#Static resources
python manage.py collectstatic